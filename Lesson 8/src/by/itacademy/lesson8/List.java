package by.itacademy.lesson8;

import java.lang.annotation.ElementType;

public interface List<E> extends Iterable<E>{

    void add(E element);
    void remove(int index)throws ArrayIndexOutOfBoundsException;
    E get(int index) throws ArrayIndexOutOfBoundsException;
    E set(int index, E element);

}
