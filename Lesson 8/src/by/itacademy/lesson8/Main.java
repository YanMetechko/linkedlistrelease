package by.itacademy.lesson8;

public class Main {

        public static void main(String[] args) {
            LinkedList<Integer> trial = new LinkedList<>();
            for (int i = 0; i < 21; i++) {
                trial.add((int)(Math.random() * 100));
            }
            LLIterator<Integer> iterator = new LLIterator(trial);
            System.out.print(trial.get(0)+" ");
            while(iterator.hasNext())
                System.out.print(iterator.next()+" ");
            System.out.println();
            trial.remove(11);
            trial.printOut();
            System.out.println();
            trial.set(15,(int)(Math.random() * 100));
            trial.printOut();

    }
}
