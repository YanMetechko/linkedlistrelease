package by.itacademy.lesson8;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class LLIterator<E> implements Iterator<E> {

    private LinkedList<E> collection;
    private int i=0;

    public LLIterator(LinkedList<E> collection) {
        this.collection = collection;
    }


    @Override
    public boolean hasNext() {

        if (collection.checkNext()) {
            return true;
        }
        return false;
    }

    @Override
    public E next() {
        if (i < (collection.getSize()-1))
            i++;
        return collection.get(i);
    }


}
