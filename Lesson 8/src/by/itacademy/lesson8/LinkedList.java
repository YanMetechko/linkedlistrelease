package by.itacademy.lesson8;

import java.util.Iterator;

public class LinkedList<E> implements List<E> {

    private int size = 0;
    private Nod<E> first, last;
    int iterationCount = 0;

    public LLIterator<E> iterator() {
        return new LLIterator(this);
    }


    private static class Nod<E> {
        E element;
        Nod<E> prev;
        Nod<E> next;

        public Nod(E element, Nod<E> prev, Nod<E> next) {
            this.element = element;
            this.prev = prev;
            this.next = next;
        }
    }


    public boolean checkNext() {
        iterationCount++;
        if (iterationCount < size)
            return true;
        return false;
    }


    public int getSize() {
        return size;
    }

    public void printOut() {
        Nod<E> temp = first;
        for (int i = 0; i < size; i++) {
            System.out.print(temp.element + " ");
            temp = temp.next;
        }
    }

    @Override
    public void add(E element) {
        if (size == 0) {
            Nod<E> newNod = new Nod<>(element, null, null);
            first = newNod;
            last = newNod;
            size = 1;
        } else {
            Nod<E> newNod = new Nod<E>(element, last, null);
            Nod<E> temp = last;
            temp.next = newNod;
            last = newNod;
            size++;
        }

    }


    @Override
    public void remove(int index) throws ArrayIndexOutOfBoundsException {
        if ((index >= 0) & (index < size)) {
            Nod<E> temp = first;
            for (int i = 0; i < index; i++)
                temp = temp.next;
            temp.prev.next = temp.next;
            temp.next.prev = temp.prev;
            size--;
            if (index == (size - 1)) last = temp.prev;
        } else throw new

                ArrayIndexOutOfBoundsException("Index is outside existing collection!");

    }

    @Override
    public E get(int index) throws ArrayIndexOutOfBoundsException {
        if ((index >= 0) & (index < size)) {
            Nod<E> temp = first;
            for (int i = 0; i < index; i++)
                temp = temp.next;
            return temp.element;
        } else throw new
                ArrayIndexOutOfBoundsException("Index is outside existing collection!");

    }

    @Override
    public E set(int index, E element) {
        E e;
        if (index == 0) {
            e = first.element;
            first.element = element;
            return e;
        } else if ((index > 0) & (index < size)) {
            Nod<E> temp = first;
            for (int i = 0; i < index; i++) {
                temp = temp.next;
            }
            e = temp.element;
            temp.element = element;
            return e;
        } else throw new ArrayIndexOutOfBoundsException("Index is outside existing collection!");
    }
}
